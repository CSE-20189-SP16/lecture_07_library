Lecture 07 - Library
====================

Source code for Library example.

# Create Shared Library
gcc -fPIC -c -o hello.o hello.c
gcc -shared -o libhello.so hello.o

# Link Shared Library
gcc -c -o main.o main.c
gcc -o hello.exe -L. main.o -lhello

# Create Static Library
gcc -c -o hello.o hello.c
ar rcs libhello.so hello.o

# Link Static Library
gcc -static -o hello.exe -L. main.o -lhello
